module gitee.com/zhenxinma/gorpc

go 1.20

require (
	gitee.com/zhenxinma/gocommon v1.3.4
	gitee.com/zhenxinma/golib v1.0.4
)

require (
	github.com/sirupsen/logrus v1.8.1 // indirect
	github.com/stretchr/testify v1.5.1 // indirect
	golang.org/x/sys v0.3.0 // indirect
)
