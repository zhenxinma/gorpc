package errcode

var RpcGoServerListenerErr = &ErrorMessage{
	Code:    2001,
	Message: "go_rpc.ServerListenerErr: server listener errcode",
}
