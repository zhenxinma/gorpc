// Package server
// Create  2023-04-05 21:50:12
package server

type ServerInterface interface {
	GetServerName() string
}
